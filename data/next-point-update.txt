CVE-2023-41887
	[bookworm] - openrefine 3.6.2-2+deb12u2
CVE-2023-41886
	[bookworm] - openrefine 3.6.2-2+deb12u2
CVE-2023-3153
	[bookworm] - ovn 23.03.1-1~deb12u1
CVE-2023-43040
	[bookworm] - ceph 16.2.11+ds-2+deb12u1
CVE-2023-40743
	[bookworm] - axis 1.4-28+deb12u1
CVE-2023-45143
	[bookworm] - node-undici 5.15.0+dfsg1+~cs20.10.9.3-1+deb12u2
CVE-2023-40481
	[bookworm] - 7zip 22.01+dfsg-8+deb12u1
CVE-2023-31102
	[bookworm] - 7zip 22.01+dfsg-8+deb12u1
CVE-2023-46586
	[bookworm] - weborf 0.19-3
CVE-2023-3724
	[bookworm] - wolfssl 5.5.4-2+deb12u1
CVE-2023-42117
	[bookworm] - exim4 4.96-15+deb12u3
CVE-2023-42119
	[bookworm] - exim4 4.96-15+deb12u3
CVE-2023-39350
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39351
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39352
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39353
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39354
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39356
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40181
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40186
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40188
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40567
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40569
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40589
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-4039
	[bookworm] - gcc-12 12.2.0-14+deb12u1
CVE-2023-45897
	[bookworm] - exfatprogs 1.2.0-1+deb12u1
